<?php defined("BASEPATH") or exit("No direct script access allowed");
  class Opros extends CI_Controller {
    
    public function index($id = NULL) {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
	  if ($id === NULL)
		echo json_encode($this->db->where('idParent IS NULL')->order_by('sort', 'ASC')->get('opros')->result_array());
	  else {
	  
	    if ($id === 'first') {
	      $id = $this->db->where('idParent IS NULL')->select('id')->order_by('sort')->limit(1)->get('opros')->row()->id;
	    }
		 
		 echo json_encode($this->db->get_where('opros', array('idRoot' => $id))->result_array());
	  }
    }
  
    public function create() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	  $this->db->insert('opros', array( "name" => "Новый опрос", "desc" => "" ));
	  $id = $this->db->insert_id();
	  $this->db->replace('opros', array( "id" => $id, "idRoot" => $id, "name" => "Новый опрос", "desc" => "Описание нового опроса" ));
	  echo $id;
    }
	
    public function createRoot() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

      $data = json_decode(file_get_contents('php://input'));
	  $this->db->insert('opros', $data);
	  $id = $this->db->insert_id();
	  $data->idRoot = $id;
	  $data->id = $id;
	  $this->db->replace('opros', $data);
	  echo $id;
    }
	
    public function createItem() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

      $data = json_decode(file_get_contents('php://input'));
	  $this->db->insert('opros', $data);
	  $id = $this->db->insert_id();
	  echo $id;
    }
	
	public function save() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $item = json_decode(file_get_contents('php://input'));
	  
	  if (isset($item->id)) $this->db->replace('opros', $item);
	  else {
		$this->db->insert('opros', $item);
		$item->id = $this->db->insert_id();
	  }
      echo $item->id;
    }
	
	public function remove($id) {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	  echo $this->db->delete('opros', array('id' => $id));
	}
	
	public function updateSort() {
	  header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	  
	  foreach( json_decode(file_get_contents('php://input')) as $item)
	    $this->db->replace('opros', $item);
	}
  
    public function do_upload()
    {
      header('Access-Control-Allow-Origin: *'); 
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

      $config['upload_path']          = './uploads/';
      $config['allowed_types']        = 'gif|jpg|png|PNG|GIF|JPG|JPEG|webm|WEBM|avi|mp4|video/mp4|video/x-m4v|AVI|MP4|webm|WEBM|m4v|M4V|ogv|OGV|ogg|OGG|video/ogg';
      $config['max_size']             = 10000000;
      $config['max_width']            = 10024;
      $config['max_height']           = 7068;
	  $config['encrypt_name'] = TRUE;

    
      $this->load->library('upload', $config);
  
      if(!file_exists($config['upload_path']))
      {
        mkdir($config['upload_path'], 0777, true);
      }
  
      if ( ! $this->upload->do_upload('userfile'))
      {
        $error = array('error' => $this->upload->display_errors());
  
        var_dump($_FILES);
        var_dump($error);
      }
      else
      {
        $data = array('upload_data' => $this->upload->data());
        //var_dump($_POST);
        //$this->db->where('id', $_POST['id'])->update('opros', array('img' => "http://opros-server.j693917.myjino.ru/uploads/".$data['upload_data']['file_name']));
        //$this->load->helper('url');
		//var_dump($this->upload->data());
		echo "http://{$_SERVER['HTTP_HOST']}/uploads/".$data['upload_data']['file_name'];
        //header( "Location:".$_POST['refer']);
      }
    }
    
  }
?>
