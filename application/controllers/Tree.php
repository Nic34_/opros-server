<?php defined("BASEPATH") or exit("No direct script access allowed");
class Tree extends CI_Controller
{

    public function index()
    {

    }

    public function save() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        $tree = json_decode(file_get_contents('php://input'));
        $this->rec($tree);
    }

    private function rec($tree) {
        $parent = $this->node_model->get_node($tree);

        if (!$parent->id || preg_match('@[A-z]@u',$parent->id)) {
            $this->db->insert('opros', $parent);
            $parent->id     = $this->db->insert_id();
        }

        $this->db->update('opros', $parent, array( "id" => $parent->id ));

        //var_dump($tree->img);
        //var_dump($parent->img);
        //echo "\n";

        $idParent = $parent->id;
        $idRoot = $parent->idRoot;

        foreach ($this->node_model->get_childrens($tree) as $item) {
            $item->idParent = $idParent;
            $item->idRoot   = $idRoot;

            $this->rec($item);
        }
    }
}