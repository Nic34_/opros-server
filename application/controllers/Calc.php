<?php defined("BASEPATH") or exit("No direct script access allowed");
  class UserCalc extends CI_Controller {
    
    public function index($id = null) {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
	  if ($id) echo json_encode($this->db->get_where('calc', array("id" => $id))->result());
      else echo json_encode($this->db->get('calc')->result_array());
    }
  
    public function create() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $data = json_decode(file_get_contents('php://input'));
	  if (!$this->db->insert('calc', $data)) { echo 0; die; }
	  echo $this->db->insert_id();
    }

    public function save() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $data = json_decode(file_get_contents('php://input'));
	  echo $this->db->where('id', $data->id)->update('calc', $data);
    }

    public function remove($id) {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

      echo $this->db->delete('calc', array('id' => $id));
    }

  }
?>
