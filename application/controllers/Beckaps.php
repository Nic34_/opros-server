<?php defined("BASEPATH") or exit("No direct script access allowed");
  class Beckaps extends CI_Controller {
    
    public function index($id = null) {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
	  if ($id) echo json_encode($this->db->get_where('beckaps', array("idOpros" => $id))->result_array());
      else echo json_encode($this->db->get('beckaps')->result_array());
    }
  
    public function create() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $data = json_decode(file_get_contents('php://input'));
	  echo $this->db->insert('beckaps', $data);
    }

    public function restore($id) {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

      $tree = $this->db->get_where('beckaps', array('id' => $id))->result()[0]->tree;
      $this->rec(json_decode($tree));
    }
	
    public function remove($id) {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

      echo $this->db->delete('beckaps', array('id' => $id));
    }

    private function rec($tree) {
      $parent = $this->node_model->get_node($tree);

      $this->db->replace('opros', $parent, array( "id" => $parent->id ));

      var_dump($tree->img);
      var_dump($parent->img);
      echo "\n";

      $idParent = $parent->id;
      $idRoot = $parent->idRoot;

      foreach ($this->node_model->get_childrens($tree) as $item) {
        $item->idParent = $idParent;
        $item->idRoot   = $idRoot;

        $this->rec($item);
      }
    }

  }
?>
