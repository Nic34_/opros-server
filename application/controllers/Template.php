<?php defined("BASEPATH") or exit("No direct script access allowed");
  class Template extends CI_Controller {
    
    public function index() {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
      echo json_encode($this->db->get('templates')->result_array());
    }
  
    public function create() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $data = json_decode(file_get_contents('php://input'));
	  echo $this->db->insert('templates', $data);
    }
	
	
  }
?>
