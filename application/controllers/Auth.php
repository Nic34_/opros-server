<?php defined("BASEPATH") or exit("No direct script access allowed");

  class Auth extends CI_Controller {
    
    public function login() {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

	  $data = json_decode(file_get_contents('php://input'));
	  $user = $this->auth_model->check($data);
	  if ($user) {
		$token = $this->token_model->create($user->id);
		echo json_encode( array( "result" => 1, "idUser" => $user->id,  "isAdmin" => $user->isAdmin ? 1 : 0, "isDemo" => $user->isDemo ? 1 : 0, "token" => $token) );
	  } else {
		echo json_encode( array( "result" => 0) );
	  }
	  //*/
    }
  
    public function create() {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: POST, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    
      $data = json_decode(file_get_contents('php://input'));
	  
	  if ($this->auth_model->find($data)) {
		  echo json_encode( array( "result" => 0, "message" => "Логин уже используется") );
		  return;
	  }
	  
	  $id = $this->auth_model->create($data);
	  $isAdmin = false; // Добавить проверку текущего для создания админа
	  if ($id) {
		$token = $this->token_model->create($id);
		echo json_encode( array( "result" => 1, "idUser" => $id,  "isAdmin" => $isAdmin, "token" => $token) );
	  } else {
		echo json_encode( array( "result" => 0) );
	  }
    }

  }
?>
