<?php defined("BASEPATH") or exit("No direct script access allowed");
class Node extends CI_Controller
{

    public function remove($id) {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        echo $this->node_model->remove($id);
    }

}