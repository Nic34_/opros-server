<?php defined("BASEPATH") or exit("No direct script access allowed");
  class UserCalc extends CI_Controller {
    
    public function index($id) {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
	  echo json_encode(array("result" => 1, "rows" => $this->usercalc_model->get_by_user_id($id)));
    }
  
    public function create() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $data = json_decode(file_get_contents('php://input'));
	  $id = $this->usercalc_model->create($data);
	  if ($id) echo json_encode(array("result" => 1, "id" => $id));
	  else echo json_encode(array("result" => 0));
    }

    public function save() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $data = json_decode(file_get_contents('php://input'));
	  echo $this->usercalc_model->update($data);
    }

    public function remove($id) {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

	  echo $this->usercalc_model->remove($id);
    }

  }
?>
