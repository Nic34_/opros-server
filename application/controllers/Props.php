<?php defined("BASEPATH") or exit("No direct script access allowed");
  class Props extends CI_Controller {
    
    public function index($id = null) {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

      echo json_encode($this->db->get('props')->result_array());
    }
  
    public function save() {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
      $data = json_decode(file_get_contents('php://input'));
	  foreach ($data as $item)
	    $this->db->replace('props', $item);
    }

    public function hasPropsCalc() {
      header('Content-type: application/json');
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

      echo json_encode($this->db->get_where('props', array('Slug'=> 'Calc'))->row_array()['value']*1);
    }
  }
?>
