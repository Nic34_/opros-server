<?php
class Opros_model extends CI_Model {

    public $id;
    public $idParent;
    public $idRoot;
    public $name;
    public $desc;
    public $img;
    public $VideoSmall;
    public $VideoBig;
    public $to;
    public $sort;

    public function get_last_ten_entries()
    {
            $query = $this->db->get('entries', 10);
            return $query->result();
    }

    public function insert_entry()
    {
            $this->title    = $_POST['title']; // please read the below note
            $this->content  = $_POST['content'];
            $this->date     = time();

            $this->db->insert('entries', $this);
    }

    public function update_entry()
    {
            $this->title    = $_POST['title'];
            $this->content  = $_POST['content'];
            $this->date     = time();

            $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

}
?>