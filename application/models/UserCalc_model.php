<?php
define('table_calc', 'calc');

class UserCalc_model extends CI_Model {

    public $id;
    public $idUser;
    public $Value;
    public $dateCreate;
    public $dateUpdate;

	public function set_data($data) {
        $this->id         = property_exists($data, "id") ? $data->id : null;
        $this->idUser     = property_exists($data, "idUser") ? $data->idUser : null;
        $this->Value      = property_exists($data, "Value") ? $data->Value : null;
        $this->dateCreate = property_exists($data, "dateCreate") ? $data->dateCreate : null;
        $this->dateUpdate = date("Y-m-d H:i:s");
	}
	
    public function get_by_user_id($idUser) {
		return $this->db->get_where(table_calc, array("idUser" => $idUser))->result_array();
    }

    public function create($data) {
		$this->set_data((object) $data);
		$this->dateCreate = date("Y-m-d H:i:s");
        if ( $this->db->insert(table_calc, $this) )
			return $this->db->insert_id();
		else return false;
    }

    public function update($data) {
		$this->set_data((object) $data);
        return $this->db->where('id', $this->id)->update(table_calc, $this);
    }
	
    public function remove($id) {
        return $this->db->delete(table_calc, array("id" => $id));
    }
}
?>