<?php
define('table_auth', 'auth');

class Auth_model extends CI_Model {

    public $id;
    public $FIO;
    public $Login;
    public $Email;
    public $Pass;
	

	public function set_data($data) {
        $this->id         = property_exists($data, "id")    ? $data->id    : null;
        $this->Login      = property_exists($data, "Login") ? $data->Login : null;
        $this->FIO        = property_exists($data, "FIO")   ? $data->FIO   : null;
        $this->Email      = property_exists($data, "Email") ? $data->Email : null;
        $this->Pass       = property_exists($data, "Pass")  ? password_hash($data->Pass, PASSWORD_DEFAULT) : null;
	}
	
    public function check($data)
    {
		$this->set_data($data);
		$user = $this->db->get_where(table_auth, array("Login" => $this->Login))->row();
        if ($user) {
			if ( password_verify( $data->Pass, $user->Pass ) ) {
				return $user;
			} else {
				return false;
			}
		} else {
			return false;
		}
    }

    public function find($data)
    {
		$this->set_data($data);
		$user = $this->db->get_where(table_auth, array("Login" => $this->Login))->row();
        if ($user) {
			return $user;
		} else {
			return false;
		}
    }

    public function create($data) {
		$this->set_data($data);
		if ($this->db->insert(table_auth, $this)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
    }

    public function remove($id) {
        return $this->db->delete(table_auth, array("id" => $id));
    }
}
?>