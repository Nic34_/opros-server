<?php
class Node_model extends CI_Model {

    public $id;
    public $idParent;
    public $idRoot;
    public $name;
    public $desc;
    public $img;
    public $VideoSmall;
    public $VideoBig;
    public $calc;
    public $to;
    public $sort;

    public function get_node($item)
    {
        $this->id         = property_exists($item, "id") && strpos($item->id, '.') === false ? $item->id : null;
        $this->idParent   = $item->idParent;
        $this->idRoot     = $item->idRoot;
        $this->name       = $item->name;
        $this->desc       = $item->desc;
        $this->img        = property_exists($item, "img") ? $item->img : null;
        $this->VideoSmall = property_exists($item, "VideoSmall") ? $item->VideoSmall : null;
        $this->VideoBig   = property_exists($item, "VideoSmall") ? $item->VideoBig : null;
        $this->calc       = property_exists($item, "calc") ? $item->calc : null;
        $this->to         = property_exists($item, "to") ? $item->to : null;
        $this->sort       = property_exists($item, "sort") ? $item->sort : null;
        return $this;
    }

    public function get_childrens($item) {
        return $item->children;
    }

    public function remove($id) {
        return $this->db->delete('opros', array("id" => $id));
    }

}
?>