<?php
define('table_token', 'token');

class Token_model extends CI_Model {

    public $id;
    public $idUser;
    public $token;
	

	public function set_data($data) {
        $this->id         = property_exists($data, "id") ? $data->id : null;
        $this->idUser     = property_exists($data, "idUser") ? $data->idUser : null;
        $this->token      = property_exists($data, "token") ? $data->token : null;
	}
	
    public function check($token)
    {
		$users = $this->db->get_where(table_token, array("token" => $this->token))->result_array();
        return count($users) ? $users[0]->idUser : false;
    }

    public function create($idUser) {
		$token = uniqid();
		$this->set_data((object) array("idUser" => $idUser, "token" => $token));
        $this->db->insert(table_token, $this);
		return $token;
    }

    public function remove($token) {
        return $this->db->delete(table_token, array("token" => $token));
    }
}
?>