<?php
  
  defined('BASEPATH') OR exit('No direct script access allowed');
  
  class Migration_Add_opros extends CI_Migration {
    
    public function up()
    {
      $this->dbforge->add_field(array(
        'id' => array(
          'type' => 'INT',
          'auto_increment' => TRUE
        ),
        'idParent' => array(
          'type' => 'INT',
          'null' => TRUE,
        ),
        'name' => array(
          'type' => 'VARCHAR',
          'constraint' => '255',
        ),
        'desc' => array(
          'type' => 'TEXT',
          'null' => TRUE,
        ),
        'img' => array(
          'type' => 'TEXT',
          'null' => TRUE,
        ),
      ));
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('opros');
    }
    
    public function down()
    {
      $this->dbforge->drop_table('opros');
    }
  }
